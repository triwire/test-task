﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public interface IWebClient
    {
        Task<string> GetStringAsync(string urlText);
    }
    public interface IHtmlParser
    {
        IHtmlDocument Parse(string htmlText);
    }

    class HtmlParser : IHtmlParser
    {
        public IHtmlDocument Parse(string htmlText)
        {
            return new HtmlDocument(htmlText);
        }
    }
    public interface IHtmlDocument
    {
        // Много интересных свойств, не критичных в рамках данной задачи
    }

    class TestWebClient : IWebClient
    {
        public Task<string> GetStringAsync(string urlText)
        {
            return Task.FromResult("test:" + urlText);
        }
    }



    class HtmlDocument : IHtmlDocument
    {
        private readonly string _html;

        public HtmlDocument(string html)
        {
            _html = html;
        }
    }


    public class Loader
    {
        private readonly IWebClient _client;
        private readonly IHtmlParser _parser;

        public Loader(IWebClient client, IHtmlParser parser)
        {
            _client = client;
            _parser = parser;
        }

        private readonly SemaphoreSlim _client_locker = new SemaphoreSlim(1);
        private readonly SemaphoreSlim _parser_locker = new SemaphoreSlim(1);

        private async Task<TResult> WithLock<TResult>(Func<Task<TResult>> func, SemaphoreSlim locker, CancellationToken cancel)
        {
            await locker.WaitAsync(cancel);
            try
            {
                return await func().ConfigureAwait(false);
            }
            finally
            {
                locker.Release();
            }
        }

        public async Task<IHtmlDocument> LoadDocumentAsync(string url, CancellationToken cancel)
        {

            var text = await WithLock(() => _client.GetStringAsync(url), _client_locker, cancel);

            return await WithLock(() => Task.FromResult(_parser.Parse(text)), _parser_locker, cancel);

        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            var loader = new Loader(new TestWebClient(), new HtmlParser());
            Task.Factory.StartNew(async () =>
            {
                var result = await loader.LoadDocumentAsync("test", CancellationToken.None);

            }).Wait();

        }
    }
}
